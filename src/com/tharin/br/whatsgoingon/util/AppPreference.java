package com.tharin.br.whatsgoingon.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.tharin.br.whatsgoingon.App;

public class AppPreference {

    private static SharedPreferences getSettingsSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }

    public static String[] getSettingsPreferences(String key) {

        SharedPreferences mSharedPreferences = getSettingsSharedPreferences();

        if (mSharedPreferences != null) {
            Set<String> keySet = mSharedPreferences.getStringSet(key, null);

            if (keySet != null)
                return keySet.toArray(new String[] {});
        }

        return null;
    }

    public static List<String> getSettingsPreferencesAsList(String key) {

        SharedPreferences mSharedPreferences = getSettingsSharedPreferences();

        if (mSharedPreferences != null) {
            Set<String> keySet = mSharedPreferences.getStringSet(key, null);

            if (keySet != null) {

                List<String> mList = new ArrayList<>();

                for (String value : keySet) {
                    mList.add(value);
                }

                return mList;
            }

        }

        return null;
    }

    public static String getStringPreference(String key) {

        SharedPreferences mSharedPreferences = getSettingsSharedPreferences();

        if (mSharedPreferences != null) {
            String keySet = mSharedPreferences.getString(key, null);

            if (keySet != null) {
                return keySet;
            }
        }

        return "";
    }

    public static boolean setSettingsPrefrence(String key, Set<String> content) {

        SharedPreferences mSharedPreferences = getSettingsSharedPreferences();

        if (mSharedPreferences != null && content != null) {

            Editor mEditor = mSharedPreferences.edit();
            mEditor.putStringSet(key, content);
            mEditor.commit();

            return true;
        }

        return false;
    }

    public static boolean setSettingsPrefrence(String key, String content) {

        SharedPreferences mSharedPreferences = getSettingsSharedPreferences();

        if (mSharedPreferences != null && content != null) {

            Editor mEditor = mSharedPreferences.edit();
            mEditor.putString(key, content);
            mEditor.commit();

            return true;
        }

        return false;
    }

}
