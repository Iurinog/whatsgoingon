package com.tharin.br.whatsgoingon.util;

import java.text.DecimalFormat;
import java.util.Calendar;

import android.content.res.Resources;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.util.Variables.Category;
import com.tharin.br.whatsgoingon.util.Variables.Cities;

public class Utils {

    public static String getXmlCityName(String name) {

        Resources resources = App.getContext().getResources();

        if (name.equalsIgnoreCase(resources.getString(R.string.city_sao_paulo))) {
            return "SaoPaulo";
        } else if (name.equalsIgnoreCase(resources
                .getString(R.string.city_rio_janeiro))) {
            return "RioDeJaneiro";
        } else if (name.equalsIgnoreCase(resources
                .getString(R.string.city_belo_horizonte))) {
            return "BeloHorizonte";
        }

        return "";
    }

    public static int getCityDrawable(String name) {

        Cities city = Variables.Cities.getEnum(name);

        if (city != null) {
            return city.getPicture();
        }

        return R.drawable.ic_launcher;
    }

    public static int getCityDrawable(int name) {

        Cities city = Variables.Cities.getEnum(name);

        if (city != null) {
            return city.getPicture();
        }

        return R.drawable.ic_launcher;
    }

    public static int getEventDrawable(String name) {

        Category category = Variables.Category.getEnum(name);

        if (category != null) {
            return category.getPicture();
        }

        return R.drawable.ic_launcher;
    }

    public static int getEventBackgroundDrawable(String name) {

        Category category = Variables.Category.getEnum(name);

        if (category != null) {
            return category.getBackgroundImage();
        }

        return R.drawable.ic_launcher;
    }

    public static String getCurrentDate() {

        Calendar mCalendar = Calendar.getInstance();
        int dayOfTheMonth = mCalendar.get(Calendar.DAY_OF_MONTH);
        int month = mCalendar.get(Calendar.MONTH) + 1;

        DecimalFormat df = new DecimalFormat("00");
        String date = df.format(dayOfTheMonth) + "/" + df.format(month);

        return date;
    }

    public static boolean compareDates(String date) {

        String currentDate = Utils.getCurrentDate();

        if (date.contains(currentDate))
            return true;

        return false;
    }

}