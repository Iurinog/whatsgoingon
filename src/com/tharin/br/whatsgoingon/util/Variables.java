package com.tharin.br.whatsgoingon.util;

import android.content.res.Resources;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;

public class Variables {

    /**
     * Splash Screen Time
     */
    public static final int SPLASH_SCREEN_TIME = 3000;

    /**
     * Server URL for connection
     */
    public static final String SERVER_URL = "http://192.168.1.104:8080/WhatsGoingOn/events/";

    /**
     * Preferences KEY
     */
    public static final String SETTINGS_CITIES_KEY = "cities";
    public static final String SETTINGS_CATEGORIES_KEY = "categories";
    public static final String SETTINGS_CATEGORIES_ORDER_KEY = "categoriesorder";
    public static final String RECEIVER_NOTIFICATION = "notification_date";
    
    /**
     * 
     */
    public static enum Category {
        FAVORITE(R.string.events_action_favorite, R.drawable.ic_star_fill,
                R.drawable.bg_default), COUNTRY(R.string.event_country,
                R.drawable.ic_country, R.drawable.bg_country), ELETRO(
                R.string.event_eletro, R.drawable.ic_eletro,
                R.drawable.bg_default), PUB(R.string.event_pub,
                R.drawable.ic_pub, R.drawable.bg_pub), THEATER(
                R.string.event_theater, R.drawable.ic_theater,
                R.drawable.bg_theater), SHOW(R.string.event_show,
                R.drawable.ic_show, R.drawable.bg_default), SAMBA(
                R.string.event_samba, R.drawable.ic_samba,
                R.drawable.bg_default);

        private int mName;
        private int mPicture;
        private int mBackgroundImage;

        Category(int name, int picture, int background) {
            mName = name;
            mPicture = picture;
            mBackgroundImage = background;
        }

        public int getName() {
            return mName;
        }

        public int getPicture() {
            return mPicture;
        }

        public int getBackgroundImage() {
            return mBackgroundImage;
        }

        public static Category getEnum(String value) {

            Resources resouce = App.getContext().getResources();

            for (Category v : values())
                if (resouce.getString(v.mName).equalsIgnoreCase(value))
                    return v;

            return null;
        }
    }

    public static enum Cities {
        SaoPaulo(R.string.city_sao_paulo, R.drawable.ic_sao_paulo), RJ(
                R.string.city_rio_janeiro, R.drawable.ic_rio_janeiro), BH(
                R.string.city_belo_horizonte, R.drawable.ic_belo_horizonte);

        private int mName;
        private int mPicture;

        Cities(int name, int picture) {
            mName = name;
            mPicture = picture;
        }

        public int getName() {
            return mName;
        }

        public int getPicture() {
            return mPicture;
        }

        public static Cities getEnum(String value) {

            Resources resouce = App.getContext().getResources();

            for (Cities v : values())
                if (resouce.getString(v.mName).equalsIgnoreCase(value))
                    return v;

            return null;
        }

        public static Cities getEnum(int value) {

            for (Cities v : values())
                if (v.mName == value)
                    return v;

            return null;
        }

    }
}
