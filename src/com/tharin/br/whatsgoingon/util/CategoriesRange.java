package com.tharin.br.whatsgoingon.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tharin.br.whatsgoingon.controller.Controller;

public class CategoriesRange {

    private List<String> mCategories;
    private Map<String, Integer[]> mCategoriesRange;

    private static CategoriesRange sCategoriesRange;

    public static CategoriesRange getInstance(List<String> categories) {

        if (sCategoriesRange == null) {
            sCategoriesRange = new CategoriesRange(categories);
        }

        return sCategoriesRange;
    }

    private CategoriesRange(List<String> categories) {
        mCategories = categories;
        mCategoriesRange = new HashMap<>();

        loadCategoriesRange();
    }

    private void loadCategoriesRange() {

        Controller controller = Controller.getInstance();

        int count = 0;

        for (String category : mCategories) {

            Integer[] array = new Integer[2];
            array[0] = count;
            count += controller.getPlacesFromCategoryCount(category);
            array[1] = count;

            mCategoriesRange.put(category, array);
            count++;
        }
    }

    public String categoryName(int value) {

        for (String category : mCategories) {

            Integer[] array = mCategoriesRange.get(category);

            if (value >= array[0] && value <= array[1])
                return category;
        }

        return "";
    }
}
