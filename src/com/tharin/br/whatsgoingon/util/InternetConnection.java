package com.tharin.br.whatsgoingon.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.tharin.br.whatsgoingon.App;

public class InternetConnection {

    public enum INTERNET_STATUS {
        TYPE_WIFI, TYPE_MOBILE, TYPE_NOT_CONNECTED
    }

    public static INTERNET_STATUS getConnectivityStatus() {
        ConnectivityManager cm = (ConnectivityManager) App.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {

            if (activeNetwork.isConnected() && activeNetwork.isAvailable()) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return INTERNET_STATUS.TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return INTERNET_STATUS.TYPE_MOBILE;

            }
        }

        return INTERNET_STATUS.TYPE_NOT_CONNECTED;
    }

    public static Document getURLContent(String link) {

        Document mDocument = null;

        try {

            URL mURL = getURLFromString(link);
            URLConnection mURLConnection = mURL.openConnection();
            mURLConnection.setConnectTimeout(3000);

            if (mURLConnection != null) {
                mDocument = Jsoup.parse(mURLConnection.getInputStream(), null,
                        link);
            }

        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mDocument;
    }

    private static URL getURLFromString(String link)
            throws MalformedURLException {
        return new URL(link);
    }

}
