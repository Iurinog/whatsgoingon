package com.tharin.br.whatsgoingon.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryComparable implements Comparator<String> {

    private List<String> mCategoryOrder;

    public CategoryComparable() {
        mCategoryOrder = AppPreference
                .getSettingsPreferencesAsList(Variables.SETTINGS_CATEGORIES_ORDER_KEY);

        readOrder();
    }

    private void readOrder() {

        if (mCategoryOrder == null) {
            return;
        }
        
        Map<Integer, String> map = new HashMap<>();
        int count = mCategoryOrder.size();

        for (int i = 0; i < count; i++) {

            String[] splitted = mCategoryOrder.get(i).split("_");

            if (splitted.length > 1)
                map.put(Integer.valueOf(splitted[0]), splitted[1]);
            else
                map.put(i, splitted[0]);
        }

        mCategoryOrder.clear();

        for (int i = 0; i < count; i++) {
            mCategoryOrder.add(map.get(i));
        }
    }

    @Override
    public int compare(String string1, String string2) {

        if (mCategoryOrder != null) {

            int string1Pos = mCategoryOrder.indexOf(string1);
            int string2Pos = mCategoryOrder.indexOf(string2);

            if (string1Pos > string2Pos)
                return 1;
            else if (string1Pos < string2Pos)
                return -1;
            else
                return 0;
        }

        return 0;
    }

}
