package com.tharin.br.whatsgoingon.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.util.Utils;

public abstract class Place {

    protected static final String STRING_EMPTY = "";

    protected static final String EVENT_DATE = "event_date";
    protected static final String EVENT_TITLE = "event_title";
    protected static final String EVENT_DESCRIPTION = "event_description";
    protected static final String EVENT_FAVORITE = "event_favorite";
    protected static final String PLACE_LAST_UPDATE = "place_last_update";

    protected String mName;
    protected String mXmlName;
    protected String mURL;
    protected int mPicture;
    protected String mLocation;
    protected String mLatitude;
    protected String mLongitude;
    protected int mCity;
    protected String mCategory;
    protected boolean isFavorite;

    /* Variable used for update event or not */
    protected boolean isUpdated;

    /* Variable used to check if this place will be showed for user or not */
    protected boolean hasEvent;

    protected Context mContext;
    protected SharedPreferences mSharedPreferences;
    protected String mLastUpdateDate;
    protected Event mEvent;

    public Place() {
        mContext = App.getContext();

        defineInfos();

        mSharedPreferences = getSharedPreferences();
        refreshEventObject();
    }

    protected abstract void defineInfos();

    protected boolean compareDates(String eventDate) {

        String currentDate = Utils.getCurrentDate();

        if (eventDate.contains(currentDate))
            return true;

        return false;
    }

    public void saveNewEvent(Event newEvent) {

        if (mSharedPreferences == null) {
            mSharedPreferences = getSharedPreferences();
        }

        // Clean all events before
        Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(EVENT_DATE, newEvent.getDate());
        mEditor.putString(EVENT_TITLE, newEvent.getTitle());
        mEditor.putString(EVENT_DESCRIPTION, newEvent.getDescription());
        mEditor.putBoolean(EVENT_FAVORITE, this.isFavorite);

        mEditor.putString(PLACE_LAST_UPDATE, Utils.getCurrentDate());

        mEditor.commit();

        refreshEventObject();
    }

    public void saveEventInfo(String key, boolean value) {

        if (mSharedPreferences == null) {
            mSharedPreferences = getSharedPreferences();
        }

        // Clean all events before
        Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean(key, value);

        mEditor.commit();
    }

    public void saveEventInfo(String key, String value) {

        if (mSharedPreferences == null) {
            mSharedPreferences = getSharedPreferences();
        }

        // Clean all events before
        Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(key, value);

        mEditor.commit();
    }

    public Event getEvent() {
        return mEvent;
    }

    public void refreshEventObject() {
        mEvent = readEventFromPreference();

        isUpdated = compareDates(mLastUpdateDate);
        hasEvent = compareDates(mEvent.getDate());
    }

    public boolean isUpdated() {
        return isUpdated;
    }

    public boolean hasEventToday() {
        return hasEvent;
    }

    public void setIsUpdated(boolean update) {
        isUpdated = update;
        mLastUpdateDate = Utils.getCurrentDate();
        saveEventInfo(PLACE_LAST_UPDATE, mLastUpdateDate);
    }

    public String getURL() {
        return mURL;
    }

    public String getLocation() {
        return mLocation;
    }

    public int getImage() {
        return mPicture;
    }

    public String getName() {
        return mName;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean favorite) {
        isFavorite = favorite;
        saveEventInfo(EVENT_FAVORITE, isFavorite);
    }

    public String getXmlName() {
        return mXmlName;
    }

    public int getCity() {
        return mCity;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public String getCategory() {
        return mCategory;
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(this.getXmlName(), 0);
    }

    public Event readEventFromPreference() {

        Event mEvent = new Event();

        if (mSharedPreferences != null) {

            mEvent.setDate(mSharedPreferences.getString(EVENT_DATE,
                    STRING_EMPTY));
            mEvent.setTitle(mSharedPreferences.getString(EVENT_TITLE,
                    STRING_EMPTY));
            mEvent.setDescription(mSharedPreferences.getString(
                    EVENT_DESCRIPTION, STRING_EMPTY));
            mEvent.setPlace(getName());

            mLastUpdateDate = mSharedPreferences.getString(PLACE_LAST_UPDATE,
                    STRING_EMPTY);
            isFavorite = mSharedPreferences.getBoolean(EVENT_FAVORITE, false);
        }

        return mEvent;
    }

    @Override
    public String toString() {
        return mXmlName;
    }

    @Override
    public boolean equals(Object o) {

        if (mXmlName.equalsIgnoreCase(o.toString()))
            return true;

        return false;
    }
}
