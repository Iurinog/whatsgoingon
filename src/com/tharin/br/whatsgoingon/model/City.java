package com.tharin.br.whatsgoingon.model;

import java.util.Arrays;
import java.util.List;

import com.tharin.br.whatsgoingon.util.Variables.Category;

public abstract class City {

    protected static Place[] empty = {};

    public List<Place> getPlaces(Category category) {
        return Arrays.asList(getClassesByCategory(category));
    }

    protected Place[] getClassesByCategory(Category category) {

        switch (category) {
        case COUNTRY:
            return empty;
        case ELETRO:
            return empty;
        case PUB:
            return empty;
        case THEATER:
            return empty;
        case SHOW:
            return empty;
        case SAMBA:
            return empty;

        default:
            return empty;
        }
    }
}
