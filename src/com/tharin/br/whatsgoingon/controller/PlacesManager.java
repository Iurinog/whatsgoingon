package com.tharin.br.whatsgoingon.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import android.content.res.Resources;
import android.util.Log;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.cities.SaoPaulo;
import com.tharin.br.whatsgoingon.model.City;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.AppPreference;
import com.tharin.br.whatsgoingon.util.Variables;
import com.tharin.br.whatsgoingon.util.Variables.Category;

public class PlacesManager {

    public static final String TAG = "PlacesManager";

    public List<Place> getPlacesList() {
        Log.d(TAG, "Gettings Places List without parameters");

        List<Place> mPlaces = new ArrayList<Place>();

        String[] cities = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CITIES_KEY);

        String[] categories = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

        if (cities != null && categories != null) {

            if (cities.length > 0 && categories.length > 0)
                mPlaces = extractPlacesFromCities(cities, categories);
        }

        return mPlaces;
    }

    public List<Place> getPlacesList(List<Place> places, String cityName,
            String categoryName) {
        Log.d(TAG, "Gettings Places List with parameters");

        List<Place> mPlaces = new ArrayList<Place>();

        Resources resource = App.getContext().getResources();

        for (Place place : places) {

            if (resource.getString(place.getCity()).equalsIgnoreCase(cityName)) {

                if (place.getCategory().equalsIgnoreCase(categoryName)) {
                    mPlaces.add(place);
                }
            }
        }

        return mPlaces;
    }

    public List<Place> getPlacesFromCity(List<Place> places, String cityName) {

        List<Place> mPlaces = new ArrayList<Place>();
        Resources resources = App.getContext().getResources();

        if (places != null) {

            for (Place place : places) {

                if (cityName.equalsIgnoreCase(resources.getString(place
                        .getCity())))
                    mPlaces.add(place);
            }
        }
        return mPlaces;
    }

    public Place getPlaceFromCity(List<Place> places, String city, int position) {

        List<Place> mPlaces = getPlacesFromCity(places, city);

        if (mPlaces != null) {
            if (mPlaces.size() > 0) {

                try {
                    Place place = mPlaces.get(position);
                    return place;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        return null;
    }

    public List<String> getCitiesList() {
        Log.d(TAG, "Getting Cities List");

        List<String> mCities = new ArrayList<String>();

        String[] cities = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CITIES_KEY);

        if (cities != null) {
            return Arrays.asList(cities);
        }

        return mCities;
    }

    public List<String> getCategoriesList() {
        Log.d(TAG, "Getting Categories List");

        List<String> mCategories = new ArrayList<String>();

        String[] categories = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

        if (categories != null) {
            mCategories.addAll(Arrays.asList(categories));

            return mCategories;
        }

        return mCategories;
    }

    public List<Place> getPlacesFavorites(List<Place> places) {

        List<Place> mPlaces = new ArrayList<Place>();

        if (places != null) {
            for (Place place : places) {

                if (place.isFavorite())
                    mPlaces.add(place);
            }
        }

        return mPlaces;
    }

    public List<Place> getPlacesFromCategory(List<Place> places, String category) {

        List<Place> mPlaces = new ArrayList<Place>();

        if (places != null) {

            if (category.equalsIgnoreCase(Variables.Category.FAVORITE.name())) {

                for (Place place : places) {

                    if (place.isFavorite())
                        mPlaces.add(place);
                }

            } else {
                for (Place place : places) {

                    if (place.getCategory().equalsIgnoreCase(category))
                        mPlaces.add(place);
                }
            }
        }

        return mPlaces;
    }

    public int getPlacesFromCategoryCount(List<Place> places, String category) {

        List<Place> mPlaces = new ArrayList<Place>();

        if (places != null) {

            if (category.equalsIgnoreCase(Variables.Category.FAVORITE.name())) {

                for (Place place : places) {

                    if (place.isFavorite())
                        mPlaces.add(place);
                }

            } else {
                for (Place place : places) {

                    if (place.getCategory().equalsIgnoreCase(category))
                        mPlaces.add(place);
                }
            }
        }

        return mPlaces.size();
    }

    public Place getPlaceFromCategory(List<Place> places, String category,
            int position) {

        List<Place> mPlaces = getPlacesFromCategory(places, category);

        if (mPlaces != null) {
            if (mPlaces.size() > 0) {

                try {
                    Place place = mPlaces.get(position);
                    return place;
                } catch (Exception e) {
                    return null;
                }
            }
        }

        return null;
    }

    public List<Place> getPlacesFromCityAndCategory(List<Place> places,
            String city, String category) {

        List<Place> mPlaces = new ArrayList<Place>();
        List<Place> placesByCity = getPlacesFromCity(places, city);

        for (Place place : placesByCity) {

            if (place.getCategory().equalsIgnoreCase(city)) {
                mPlaces.add(place);
            }
        }

        return mPlaces;
    }

    private List<Place> extractPlacesFromCities(String[] cities,
            String[] categories) {

        List<Place> mPlaces = new ArrayList<Place>();

        List<City> mCities = getCitiesList(cities);
        List<Category> mCategories = getCategoryList(categories);

        for (City city : mCities) {

            for (Category category : mCategories) {
                mPlaces.addAll(city.getPlaces(category));
            }
        }

        return mPlaces;
    }

    private List<City> getCitiesList(String[] cities) {

        List<City> mCities = new ArrayList<City>();

        for (String cityName : cities) {

            City mCity = getCityFromName(cityName);

            if (mCity != null)
                mCities.add(mCity);
        }

        return mCities;
    }

    private City getCityFromName(String name) {

        Resources resources = App.getContext().getResources();

        if (name.equalsIgnoreCase(resources.getString(R.string.city_sao_paulo))) {
            return new SaoPaulo();
        } else if (name.equalsIgnoreCase(resources
                .getString(R.string.city_rio_janeiro))) {
            return null;
        } else if (name.equalsIgnoreCase(resources
                .getString(R.string.city_belo_horizonte))) {
            return null;
        }

        return null;
    }

    private List<Category> getCategoryList(String[] categories) {

        List<Category> mCategories = new ArrayList<Category>();

        for (String categoryName : categories) {

            Category mCategory = getCategoryFromName(categoryName);

            if (mCategory != null)
                mCategories.add(mCategory);
        }

        return mCategories;

    }

    private Category getCategoryFromName(String name) {

        name = name.toUpperCase(Locale.getDefault());

        Category mCategory = null;

        try {
            mCategory = Category.valueOf(name);
        } catch (Exception e) {
            return null;
        }

        return mCategory;
    }
}