package com.tharin.br.whatsgoingon.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.os.AsyncTask;

import com.tharin.br.whatsgoingon.model.Event;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.InternetConnection;
import com.tharin.br.whatsgoingon.util.InternetConnection.INTERNET_STATUS;
import com.tharin.br.whatsgoingon.util.Utils;
import com.tharin.br.whatsgoingon.util.Variables;

public class UpdateEvents extends AsyncTask<String, String, Integer> {

    /**
     * Activities who will listen for events update. They will be called when
     * update start and finish.
     */
    private List<PropertyChangeListener> mActivitiesListener;

    private Controller mController;
    private List<Place> mPlaces;
    private List<String> mCategories;
    private List<String> mCities;

    // Unique Instance
    private static UpdateEvents sUpdateEvents;

    public static UpdateEvents getInstace() {
        if (sUpdateEvents == null)
            sUpdateEvents = new UpdateEvents();

        return sUpdateEvents;
    }

    private UpdateEvents() {
        mActivitiesListener = new ArrayList<PropertyChangeListener>();
    }

    private void notifyListeners(Object object, String property,
            String oldValue, String newValue) {

        for (PropertyChangeListener name : mActivitiesListener) {
            name.propertyChange(new PropertyChangeEvent(object, property,
                    oldValue, newValue));
        }
    }

    public boolean isAppRunning() {
        return mActivitiesListener.size() > 1;
    }

    public void addChangeListener(PropertyChangeListener newListener) {

        String objName = newListener.getClass().getName();

        for (PropertyChangeListener propertiesListenner : mActivitiesListener) {

            if (objName.equalsIgnoreCase(propertiesListenner.getClass()
                    .getName()))
                return;
        }

        mActivitiesListener.add(newListener);
    }

    public void removeChangeListener(PropertyChangeListener listener) {

        String objName = listener.getClass().getName();

        if (mActivitiesListener.contains(objName)) {
            mActivitiesListener.remove(objName);
        }
    }

    private static UPDATE_STATUS mUpdateStatus;

    /**
     * Flag control for events update
     */
    public static enum UPDATE_STATUS {
        NONE(0), START(1), UPDATING(2), FINISHED(5), ERROR_INTERNET(-1), ERROR_SERVER(
                -2), ERROR_PARSER(-3), ERROR_PLACES(-4), ERROR_OTHER(-10);

        private int mFlag;

        private UPDATE_STATUS(int flag) {
            mFlag = flag;
        }

        public int getValue() {
            return mFlag;
        }

        public String getValeuString() {
            return String.valueOf(mFlag);
        }

        public static UPDATE_STATUS getUpdateFlag(int number) {

            switch (number) {
            case 0:
                return NONE;
            case 1:
                return START;
            case 2:
                return UPDATING;
            case 5:
                return FINISHED;
            case -1:
                return ERROR_INTERNET;
            case -2:
                return ERROR_SERVER;
            case -3:
                return ERROR_PARSER;
            case -4:
                return ERROR_PLACES;
            case -10:
                return ERROR_OTHER;
            default:
                return NONE;
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mController = Controller.getInstance();

        mPlaces = mController.getPlaces();
        mCategories = mController.getCategories();
        mCategories.remove(Variables.Category.FAVORITE.name());
        mCities = mController.getCities();
    }

    @Override
    protected Integer doInBackground(String... params) {

        int progress = 1;
        int progressValue = 0;

        mUpdateStatus = UPDATE_STATUS.START;
        publishProgress(new String[] { mUpdateStatus.getValeuString(),
                String.valueOf(progress), "", "" });

        // First of all, check if sync will be necessary
        // Check if it is already updated
        if (mUpdateStatus == UPDATE_STATUS.FINISHED) {
            mUpdateStatus = UPDATE_STATUS.FINISHED;
            return mUpdateStatus.getValue();
        }

        if (InternetConnection.getConnectivityStatus() == INTERNET_STATUS.TYPE_NOT_CONNECTED) {
            mUpdateStatus = UPDATE_STATUS.ERROR_INTERNET;
            return mUpdateStatus.getValue();
        }

        if (mPlaces == null || mPlaces.isEmpty()) {
            mUpdateStatus = UPDATE_STATUS.ERROR_OTHER;
            return mUpdateStatus.getValue();
        }

        if (checkEventsUpdate()) {
            mUpdateStatus = UPDATE_STATUS.FINISHED;
            return mUpdateStatus.getValue();
        }

        if (!mCities.isEmpty() && !mCategories.isEmpty()) {
            progressValue = 100 / (mCities.size() * mCategories.size());
        } else {
            mUpdateStatus = UPDATE_STATUS.NONE;
            return mUpdateStatus.getValue();
        }

        for (String cityName : mCities) {
            for (String categoryName : mCategories) {

                if (isCancelled()) {
                    mUpdateStatus = UPDATE_STATUS.NONE;
                    return mUpdateStatus.getValue();
                }

                mUpdateStatus = UPDATE_STATUS.UPDATING;
                publishProgress(new String[] { mUpdateStatus.getValeuString(),
                        String.valueOf(progress), cityName, categoryName });

                String url = getUrlToDownload(cityName, categoryName);
                Document mDocument = null;

                if (url != null && !url.isEmpty()) {
                    mDocument = InternetConnection.getURLContent(url);
                }

                if (mDocument == null) {
                    mUpdateStatus = UPDATE_STATUS.ERROR_SERVER;
                    return mUpdateStatus.getValue();
                }

                List<Event> events = xmlParser(mDocument);

                if (events == null) {
                    mUpdateStatus = UPDATE_STATUS.ERROR_PARSER;
                    return mUpdateStatus.getValue();
                }

                // Update Events
                updateEvents(events);
                progress += progressValue;
            }
        }

        // For all events, refresh their information
        for (Place place : mPlaces) {
            place.setIsUpdated(true);
            place.refreshEventObject();
        }

        mUpdateStatus = UPDATE_STATUS.FINISHED;
        return mUpdateStatus.getValue();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        notifyListeners(values[0], values[1], values[2], values[3]);
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        UPDATE_STATUS mUpdateStatus = UPDATE_STATUS.getUpdateFlag(result);

        mController.refreshEvents();

        notifyListeners(mUpdateStatus.getValeuString(),
                mUpdateStatus.getValeuString(), "", "");
    }

    /**
     * Check if all events are updated
     * 
     * @return
     */
    private boolean checkEventsUpdate() {

        for (Place place : mPlaces) {

            if (!place.isUpdated()) {
                return false;
            }
        }

        return true;
    }

    private void updateEvents(List<Event> events) {

        for (Event event : events) {

            String placeName = event.getPlace();
            Place place = getEventByName(placeName);

            if (place != null) {
                place.saveNewEvent(event);
            }
        }
    }

    private Place getEventByName(String name) {

        for (Place place : mPlaces) {

            if (place.getName().equalsIgnoreCase(name))
                return place;
        }

        return null;
    }

    private String getUrlToDownload(String cityName, String categoryName) {

        String url = new String(Variables.SERVER_URL);
        url += "event/" + Utils.getXmlCityName(cityName) + "/" + categoryName;

        return url;
    }

    private List<Event> xmlParser(Document document) {

        List<Event> events = new ArrayList<Event>();

        if (document != null) {

            Elements mElements = document.getElementsByTag("event");

            for (int i = 0; i < mElements.size(); i++) {

                Element mElement = mElements.get(i);

                String eventDate = mElement.getElementsByTag("date").text();
                String description = mElement.getElementsByTag("description")
                        .text();
                String title = mElement.getElementsByTag("title").text();
                String place = mElement.getElementsByTag("place").text();

                Event mEvent = new Event();
                mEvent.setDate(eventDate);
                mEvent.setTitle(title);
                mEvent.setDescription(description);
                mEvent.setPlace(place);
                // TODO mEvent.setInternalDate(Utils.getCurrentDate());

                events.add(mEvent);
            }
        }

        return events;
    }

}