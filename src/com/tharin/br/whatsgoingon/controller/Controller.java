package com.tharin.br.whatsgoingon.controller;

import java.util.List;

import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.AppPreference;
import com.tharin.br.whatsgoingon.util.Variables;

public class Controller {

    /**
     * Unique instance of {@link Controller}
     */
    private static Controller sController;

    /**
     * Singleton Class
     * 
     * @return
     */
    public static Controller getInstance() {

        if (sController == null)
            sController = new Controller();

        return sController;
    }

    /**
     * User Configurations variables
     */
    private PlacesManager mPlacesManager;
    private List<String> mCities;
    private List<String> mCategories;
    private List<Place> mPlaces;

    private boolean arePlacesUpdated = false;
    private boolean isAnyPlaceUpdated = false;

    private Controller() {
        mPlacesManager = new PlacesManager();
        refreshEvents();
    };

    public void refreshEvents() {
        mCities = mPlacesManager.getCitiesList();
        mCategories = mPlacesManager.getCategoriesList();
        mPlaces = mPlacesManager.getPlacesList();

        /**
         * Check if there is any place updated, and if all places are updated
         */
        isAnyPlaceUpdated = false;
        arePlacesUpdated = true;

        for (Place place : mPlaces) {

            if (place.isUpdated()) {
                isAnyPlaceUpdated = true;
            } else {
                arePlacesUpdated = false;
            }
        }
    }

    public List<Place> getPlaces() {
        return mPlaces;
    }

    public List<String> getCities() {
        return mCities;
    }

    public List<String> getCategories() {
        return mCategories;
    }

    public List<Place> getPlacesFromCity(String cityName) {
        return mPlacesManager.getPlacesFromCity(mPlaces, cityName);
    }

    public Place getPlaceFromCity(String cityName, int position) {
        return mPlacesManager.getPlaceFromCity(mPlaces, cityName, position);
    }

    public List<Place> getPlacesFromCategory(String category) {
        return mPlacesManager.getPlacesFromCategory(mPlaces, category);
    }

    public List<Place> getPlacesFromCityAndCategory(String city, String category) {
        return mPlacesManager.getPlacesFromCityAndCategory(mPlaces, city,
                category);
    }

    public int getPlacesFromCategoryCount(String category) {
        return mPlacesManager.getPlacesFromCategoryCount(mPlaces, category);
    }

    public Place getPlaceFromCategory(String category, int position) {
        return mPlacesManager.getPlaceFromCategory(mPlaces, category, position);
    }

    /**
     * Method used to check if user configurations are empty. <br>
     * Defines if SettingsAcitivty should be called
     * 
     * @return
     */
    public boolean isUserConfigurationsEmpty() {

        String[] preference = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

        if (preference == null) {
            return true;
        }

        preference = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CITIES_KEY);

        if (preference == null) {
            return true;
        }

        return false;
    }

    public boolean areEventsUpdated() {
        return arePlacesUpdated;
    }

    /**
     * Method used to check if there is Events in history. <br>
     * Defines if Should download or not before start in MainActivity
     * 
     * @return
     */
    public boolean isUserHasHistory() {
        return isAnyPlaceUpdated;
    }

    public void setIsUserHasHistory(boolean value) {
        isAnyPlaceUpdated = value;
    }

}
