package com.tharin.br.whatsgoingon;

import android.content.Context;

public class App {
    private static App sApp;
    private static Context sContext;

    public static App getInstance() {

        if (sApp == null)
            sApp = new App();

        return sApp;
    }

    public static void setContext(Context context) {

        if (sContext == null)
            sContext = context;
    }

    public static Context getContext() {
        return sContext;
    }
}