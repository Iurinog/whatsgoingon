package com.tharin.br.whatsgoingon.cities;


import com.tharin.br.whatsgoingon.cities.sp.Country_VillaCountry;
import com.tharin.br.whatsgoingon.cities.sp.Country_VillaMix;
import com.tharin.br.whatsgoingon.cities.sp.Pub_Rhino;
import com.tharin.br.whatsgoingon.model.City;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.Variables.Category;

public class SaoPaulo extends City {

    private static Place[] country = { new Country_VillaMix(),
            new Country_VillaCountry() };
    private static Place[] eletro = {};
    private static Place[] pub = {new Pub_Rhino()};

    public SaoPaulo() {
    }

    @Override
    protected Place[] getClassesByCategory(Category category) {

        switch (category) {
        case COUNTRY:
            return country;
        case ELETRO:
            return eletro;
        case PUB:
            return pub;
        case THEATER:
            return empty;
        case SHOW:
            return empty;
        case SAMBA:
            return empty;

        default:
            return empty;
        }
    }
}
