package com.tharin.br.whatsgoingon.cities.sp;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.Variables;

public class Country_VillaCountry extends Place {

    @Override
    protected void defineInfos() {
        mName = "Villa Country";
        mURL = "http://www.villacountry.com.br/villa/Agenda_int.asp?c_data=";
        mLocation = "Av. Francisco Matarazzo, 774 - �gua Branca - S�o Paulo - (11) 3868-5858";
        mPicture = R.drawable.ic_launcher;
        mXmlName = "sp_villacountry";
        mCity = Variables.Cities.SaoPaulo.getName();
        mCategory = Variables.Category.COUNTRY.name();
        mLatitude = "-23.527905";
        mLongitude = "-46.670358";
    }

}
