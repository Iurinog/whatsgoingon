package com.tharin.br.whatsgoingon.cities.sp;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.Variables;

public class Pub_Rhino extends Place {

    @Override
    protected void defineInfos() {
        mName = "Rhino Pub";
        mURL = "http://www.rhinopub.com.br/new/eventos-3/agenda-mensal";
        mLocation = "Av. Cotovia, 99 - Indian�polis - S�o Paulo - (11) 5041-4188";
        mPicture = R.drawable.ic_launcher;
        mXmlName = "sp_rhino";
        mCity = Variables.Cities.SaoPaulo.getName();
        mCategory = Variables.Category.PUB.name();
        mLatitude = "-23.60443";
        mLongitude = "-46.674011";
    }
}
