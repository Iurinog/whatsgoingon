package com.tharin.br.whatsgoingon.cities.sp;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.Variables;

public class Country_VillaMix extends Place {

    @Override
    public void defineInfos() {
        mName = "Villa Mix";
        mURL = "http://www.villamixsp.com.br/programacao.php";
        mLocation = "Rua Beira Rio, 116 - Vila Ol�mpia - S�o Paulo - (11) 3044-3241";
        mPicture = R.drawable.sp_villa_mix;
        mXmlName = "sp_villamix";
        mCity = Variables.Cities.SaoPaulo.getName();
        mCategory = Variables.Category.COUNTRY.name();
        mLatitude = "-23.5947371";
        mLongitude = "-46.6891949";
    }

}
