package com.tharin.br.whatsgoingon.ui;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.PlacesManager;

public class SettingsFragment extends PreferenceFragment {

    private boolean isPreferenceChaged = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_preference);
    }

    @Override
    public void onResume() {
        super.onResume();
        isPreferenceChaged = false;
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(
                        onSharedPreferenceChange);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(
                        onSharedPreferenceChange);
    }

    public boolean getIsPreferenceChanged() {
        return isPreferenceChaged;
    }

    /**
     * Always when preferences changes, notify {@link PlacesManager} to update
     * the list of cities and events
     * */
    OnSharedPreferenceChangeListener onSharedPreferenceChange = new OnSharedPreferenceChangeListener() {

        @Override
        public void onSharedPreferenceChanged(
                SharedPreferences sharedPreferences, String key) {
            isPreferenceChaged = true;
        }
    };
}
