package com.tharin.br.whatsgoingon.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.model.Event;
import com.tharin.br.whatsgoingon.model.Place;

public class EventFragment extends Fragment {

    private int mPosition;
    private String mCategory;
    private String mCity;
    private View mView;

    private Place mPlace;

    public static EventFragment newInstance(int position, String category,
            String city) {

        EventFragment f = new EventFragment();
        f.mPosition = position;
        f.mCategory = category;
        f.mCity = city;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_event, container, false);

        loadView();
        loadButtons();

        return mView;
    }

    private void loadView() {

        if (mCategory == null) {
            mPlace = Controller.getInstance()
                    .getPlaceFromCity(mCity, mPosition);
        } else {
            mPlace = Controller.getInstance().getPlaceFromCategory(mCategory,
                    mPosition);
        }

        if (mPlace != null) {

            TextView eventName = (TextView) mView.findViewById(R.id.event_name);
            TextView eventDescription = (TextView) mView
                    .findViewById(R.id.event_description);
            TextView placeEventDate = (TextView) mView
                    .findViewById(R.id.event_date);
            TextView placeCity = (TextView) mView.findViewById(R.id.event_city);
            ImageView placeImage = (ImageView) mView
                    .findViewById(R.id.event_image);

            TextView placeFavoriteText = (TextView) mView
                    .findViewById(R.id.event_favorite_text);
            ImageView placeFavoriteImage = (ImageView) mView
                    .findViewById(R.id.event_favorite_image);

            if (mPlace.isFavorite()) {
                placeFavoriteText
                        .setText(R.string.events_place_is_favorite);
                placeFavoriteImage.setImageResource(R.drawable.ic_star_fill);
            } else {
                placeFavoriteText.setText(R.string.events_place_mark_favorite);
                placeFavoriteImage.setImageResource(R.drawable.ic_star);
            }

            Event event = mPlace.getEvent();

            eventName.setText(event.getTitle());
            eventDescription.setText(event.getDescription());
            placeEventDate.setText(event.getDate());
            placeCity.setText(mPlace.getCity());
            placeImage.setImageResource(mPlace.getImage());

            TextView placeLocation = (TextView) mView
                    .findViewById(R.id.event_location);
            placeLocation.setText(mPlace.getLocation());

            // ActionBar mActionBar = getActivity().getActionBar();
            // Resources resources = getResources();
            // mActionBar.setIcon(resources.getDrawable(mPlace.getImage()));
            // mActionBar.setTitle(event.getTitle());

        }
    }

    private void loadButtons() {

        ImageView mGooglePlusShare = (ImageView) mView
                .findViewById(R.id.event_google_plus);
        mGooglePlusShare.setOnClickListener(mGooglePlusClickListener);

        RelativeLayout mMapsLayout = (RelativeLayout) mView
                .findViewById(R.id.event_layout_maps);
        mMapsLayout.setOnClickListener(mMapsClickListener);

        RelativeLayout mFavoriteLayout = (RelativeLayout) mView
                .findViewById(R.id.event_layout_favorite);
        mFavoriteLayout.setOnClickListener(mFavoriteClickListener);

    }

    private OnClickListener mMapsClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            String placeLocation = "geo:" + mPlace.getLatitude() + ","
                    + mPlace.getLongitude() + "?q=" + mPlace.getName();

            Uri geoLocation = Uri.parse(placeLocation);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(geoLocation);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    };

    private OnClickListener mFavoriteClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            if (mPlace.isFavorite()) {
                mPlace.setIsFavorite(false);
            } else {
                mPlace.setIsFavorite(true);
            }

            TextView placeFavoriteText = (TextView) mView
                    .findViewById(R.id.event_favorite_text);
            ImageView placeFavoriteImage = (ImageView) mView
                    .findViewById(R.id.event_favorite_image);

            if (mPlace.isFavorite()) {
                placeFavoriteText
                        .setText(R.string.events_place_is_favorite);
                placeFavoriteImage.setImageResource(R.drawable.ic_star_fill);
            } else {
                placeFavoriteText.setText(R.string.events_place_mark_favorite);
                placeFavoriteImage.setImageResource(R.drawable.ic_star);
            }
        }
    };

    private OnClickListener mGooglePlusClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub

        }
    };

}
