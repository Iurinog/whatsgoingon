package com.tharin.br.whatsgoingon.ui.helper;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.util.Utils;

public class DrawerExpandableAdapter extends BaseExpandableListAdapter {

    private int[] mHeadersName = { R.string.events, R.string.cities,
            R.string.action_settings };

    private int[] childSettingsList = { R.string.action_settings };

    private SparseArray<List<String>> mData;
    private Context mContext;

    public DrawerExpandableAdapter(List<String> cities, List<String> categories) {

        mContext = App.getContext();
        
        mData = new SparseArray<List<String>>();
        mData.put(mHeadersName[0], categories);
        mData.put(mHeadersName[1], cities);

        // Last child - Settings
        List<String> settingsChildList = new ArrayList<String>();
        settingsChildList.add(mContext.getResources().getString(
                childSettingsList[0]));

        mData.put(mHeadersName[2], settingsChildList);
    }

    @Override
    public int getGroupCount() {
        return mHeadersName.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mData.get(mHeadersName[groupPosition]).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mHeadersName[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mData.get(mHeadersName[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = infalInflater.inflate(R.layout.drawer_header, null);

        }

        TextView mText = (TextView) convertView
                .findViewById(R.id.drawer_group_text);
        mText.setText(mHeadersName[groupPosition]);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_child, null);
        }

        TextView mText = (TextView) convertView
                .findViewById(R.id.drawer_child_name);
        ImageView mImage = (ImageView) convertView
                .findViewById(R.id.drawer_child_image);

        String text = mData.get(mHeadersName[groupPosition]).get(childPosition);

        Drawable img = mContext.getResources().getDrawable(
                R.drawable.ic_launcher);

        if (groupPosition == 2) {
            img = mContext.getResources().getDrawable(
                    android.R.drawable.ic_menu_preferences);
        } else if (groupPosition == 1) {
            img = mContext.getResources().getDrawable(
                    Utils.getCityDrawable(text));
        } else if (groupPosition == 0) {
            img = mContext.getResources().getDrawable(
                    Utils.getEventDrawable(text));
        }

        mText.setText(text);
        mImage.setImageDrawable(img);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
