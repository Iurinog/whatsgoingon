package com.tharin.br.whatsgoingon.ui.helper;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.model.Event;
import com.tharin.br.whatsgoingon.model.Place;

public class EventsAdapter extends BaseAdapter {

    private Context mContext;

    private List<Place> mPlaces;
    private String mCategory;

    public EventsAdapter(String category) {
        mCategory = category;
        mContext = App.getContext();

        mPlaces = Controller.getInstance().getPlacesFromCategory(mCategory);
    }

    @Override
    public int getCount() {
        return mPlaces.size();
    }

    @Override
    public Object getItem(int position) {
        return mPlaces.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(
                    R.layout.main_event_expandable_child, null);
        }

        TextView placeName = (TextView) convertView
                .findViewById(R.id.main_event_place_name);
        TextView placeCity = (TextView) convertView
                .findViewById(R.id.main_event_place_city);
        TextView placeEvent = (TextView) convertView
                .findViewById(R.id.main_event_place_event);
        TextView placeEventDate = (TextView) convertView
                .findViewById(R.id.main_event_place_date);
        ImageView placeImage = (ImageView) convertView
                .findViewById(R.id.main_event_image);

        Place place = mPlaces.get(position);
        Event event = place.getEvent();

        placeName.setText(place.getName());
        placeCity.setText(place.getCity());
        placeEvent.setText(event.getTitle());
        placeEventDate.setText(event.getDate());
        placeImage.setImageResource(place.getImage());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
