package com.tharin.br.whatsgoingon.ui;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.util.AppPreference;
import com.tharin.br.whatsgoingon.util.Variables;

public class SettingsActivity extends Activity {

    public static final String TAG = "SettingsActivity";

    public static final int CONFIG_RESULT_NOT_CHANGED = 100;
    public static final int CONFIG_RESULT_ADDED = 101;
    public static final int CONFIG_RESULT_REMOVED = 102;

    private SettingsFragment mSettingsFragment;

    /**
     * Views
     */
    private FrameLayout mFrameLayout;
    private Button mButtonConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mSettingsFragment = new SettingsFragment();

        mFrameLayout = (FrameLayout) findViewById(R.id.frame_settings_fragment);
        mButtonConfirm = (Button) findViewById(R.id.button_settings_fragment);
        mButtonConfirm.setOnClickListener(mOnClickListener);

        setSettingsFragment();
    }

    @Override
    public void onBackPressed() {

        if (checkEmptyConfigurations()) {
            return;
        }

        int resultCode = setResultValue();
        setResult(resultCode);

        if (resultCode != CONFIG_RESULT_NOT_CHANGED) {
            Controller.getInstance().refreshEvents();
        }

        super.onBackPressed();
    }

    private void setSettingsFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();

        fragmentTransaction.add(mFrameLayout.getId(), mSettingsFragment);
        fragmentTransaction.commit();
    }

    private boolean checkEmptyConfigurations() {

        if (mSettingsFragment != null) {

            String[] preferences = AppPreference
                    .getSettingsPreferences(Variables.SETTINGS_CITIES_KEY);

            if (preferences == null) {
                presentToast(R.string.settings_cities_empty);
                return true;
            } else {
                if (preferences.length == 0) {
                    presentToast(R.string.settings_cities_empty);
                    return true;
                }
            }

            preferences = AppPreference
                    .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

            if (preferences == null) {
                presentToast(R.string.settings_categories_empty);
                return true;
            } else {
                if (preferences.length == 0) {
                    presentToast(R.string.settings_categories_empty);
                    return true;
                }
            }
        }

        return false;
    }

    private boolean isUpdateEventsNeeded(List<String> settingsList,
            List<String> preferenceList) {

        for (String preferenceItem : preferenceList) {

            if (!settingsList.contains(preferenceItem))
                return true;
        }

        return false;
    }

    private void presentToast(int messageResource) {

        String message = App.getContext().getString(messageResource);
        Toast.makeText(App.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Set Result value for return
     */
    private int setResultValue() {

        // Check if update will be needed
        if (mSettingsFragment.getIsPreferenceChanged()) {

            if (mSettingsFragment != null) {

                // First, check for categories changed
                String[] preferenceList = AppPreference
                        .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

                List<String> settingsList = Controller.getInstance()
                        .getCategories();

                if (settingsList.isEmpty()) {
                    return CONFIG_RESULT_ADDED;
                }

                settingsList.remove(Variables.Category.FAVORITE.name());

                if (preferenceList.length > settingsList.size()) {
                    return CONFIG_RESULT_ADDED;
                } else if (preferenceList.length < settingsList.size()) {
                    return CONFIG_RESULT_REMOVED;
                } else {
                    if (isUpdateEventsNeeded(settingsList,
                            Arrays.asList(preferenceList))) {
                        return CONFIG_RESULT_ADDED;
                    }
                }

                // Second, check for Cities changed
                preferenceList = AppPreference
                        .getSettingsPreferences(Variables.SETTINGS_CITIES_KEY);

                settingsList = Controller.getInstance().getCities();

                if (settingsList.isEmpty()) {
                    return CONFIG_RESULT_ADDED;
                }

                if (preferenceList.length > settingsList.size()) {
                    return CONFIG_RESULT_ADDED;
                } else if (preferenceList.length < settingsList.size()) {
                    return CONFIG_RESULT_REMOVED;
                } else {
                    if (isUpdateEventsNeeded(settingsList,
                            Arrays.asList(preferenceList))) {
                        return CONFIG_RESULT_ADDED;
                    }
                }

            } else {
                return CONFIG_RESULT_NOT_CHANGED;
            }
        } else {
            return CONFIG_RESULT_NOT_CHANGED;
        }

        return CONFIG_RESULT_NOT_CHANGED;
    }

    private View.OnClickListener mOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            if (checkEmptyConfigurations())
                return;

            int resultCode = setResultValue();
            setResult(resultCode);

            if (resultCode != CONFIG_RESULT_NOT_CHANGED) {
                Controller.getInstance().refreshEvents();
            }

            finish();
        }
    };
}
