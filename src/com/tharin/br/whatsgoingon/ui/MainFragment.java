package com.tharin.br.whatsgoingon.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.ui.helper.EventsAdapter;
import com.tharin.br.whatsgoingon.util.Utils;
import com.tharin.br.whatsgoingon.util.Variables.Category;

public class MainFragment extends Fragment {

    public static final String TAG = "WhatsGoingOn.MainFragment";

    private View mView;
    private ListView mPlacesListView;
    private RelativeLayout mLayout;
    private TextView mEmptyEventText;

    private EventsAdapter mEventsAdapter;

    private String mCategory;

    public static MainFragment newInstance(String category) {

        MainFragment mainFragment = new MainFragment();
        mainFragment.mCategory = category;

        return mainFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_main, container, false);

        loadViews();
        loadContent();

        return mView;
    }

    public String getCategory() {
        return mCategory;
    }

    private void loadViews() {

        if (mView != null) {

            mLayout = (RelativeLayout) mView.findViewById(R.id.main_fragment);
            mPlacesListView = (ListView) mView
                    .findViewById(R.id.main_list_events);
            mEmptyEventText = (TextView) mView.findViewById(R.id.main_no_event);
        }
    }

    private void loadContent() {
        if (mCategory != null) {

            mLayout.setBackgroundResource(Utils
                    .getEventBackgroundDrawable(mCategory));

            mEventsAdapter = new EventsAdapter(mCategory);
            mPlacesListView.setAdapter(mEventsAdapter);
            mPlacesListView.setOnItemClickListener(mOnItemClickListener);

            verifyQuantityOfEvents();
        } else {
            mEmptyEventText.setVisibility(View.VISIBLE);
        }
    }

    private void verifyQuantityOfEvents() {

        int quantityEvents = Controller.getInstance()
                .getPlacesFromCategoryCount(mCategory);

        if (quantityEvents <= 0) {

            mEmptyEventText.setVisibility(View.VISIBLE);

            if (mCategory.equalsIgnoreCase(Category.FAVORITE.name())) {
                mEmptyEventText.setText(R.string.events_no_favorite_event);
            }
        }
    }

    private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {

            Intent mIntent = new Intent(getActivity(), EventsActivity.class);
            mIntent.putExtra(EventsActivity.CATEGORY_KEY, mCategory);
            mIntent.putExtra(EventsActivity.POSITION_KEY, position);
            startActivity(mIntent);
        }
    };
}
