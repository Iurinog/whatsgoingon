package com.tharin.br.whatsgoingon.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.controller.UpdateEvents;
import com.tharin.br.whatsgoingon.controller.UpdateEvents.UPDATE_STATUS;
import com.tharin.br.whatsgoingon.util.InternetConnection;
import com.tharin.br.whatsgoingon.util.Variables;

public class SplashActivity extends Activity implements PropertyChangeListener {

    public static final String TAG = "SplashScreen";

    private final int NO_MESSAGE = 0;

    private ProgressBar mProgressBar;
    private TextView mProgressText;

    private boolean isActive;

    private Controller mController;

    private Intent sSettingsIntent;
    private Intent sMainIntent;

    UpdateEvents mUpdateEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splah);

        // Set the Context for all application
        App.setContext(getApplicationContext());

        mController = Controller.getInstance();
        mUpdateEvents = UpdateEvents.getInstace();

        sMainIntent = new Intent(App.getContext(), MainActivity.class);

        // Splash delay
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                initApp();
            }
        }, Variables.SPLASH_SCREEN_TIME);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
        mUpdateEvents.addChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
        mUpdateEvents.removeChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == SettingsActivity.CONFIG_RESULT_ADDED)
            initApp();
    }

    /**
     * Initialize application <br>
     */
    private void initApp() {

        // Check for empty user configurations
        if (mController.isUserConfigurationsEmpty()) {

            // Call settings Activity to define user configurations
            startUserConfigurationsEmptyDialog();

        } else {

            // Check if all events are updated
            if (mController.areEventsUpdated()) {
                callMainActivity(NO_MESSAGE);
            } else {
                refreshSplashViews(R.string.splash_progresstext_check_internet,
                        -1);

                if (InternetConnection.getConnectivityStatus() != InternetConnection.INTERNET_STATUS.TYPE_NOT_CONNECTED) {
                    startProcessToUpdateEvents();
                } else {

                    // Check if user has none event
                    if (mController.isUserHasHistory()) {
                        callMainActivity(R.string.splash_progresstext_events_in_history);
                    } else {
                        refreshSplashViews(
                                R.string.splash_dialog_internet_message_fail,
                                -1);
                    }
                }
            }
        }
    }

    private void callMainActivity(int messageId) {

        if (messageId != NO_MESSAGE) {
            Toast.makeText(App.getContext(), messageId, Toast.LENGTH_SHORT)
                    .show();
        }

        startActivityForResult(sMainIntent, 0);
    }

    private void startProcessToUpdateEvents() {

        if (mProgressBar == null || mProgressText == null)
            loadSplashViews();

        refreshSplashViews(R.string.splash_progresstext_starting_update, 1);

        Status mUpdateEventsStatus = mUpdateEvents.getStatus();

        if (mUpdateEventsStatus != Status.RUNNING) {
            try {
                mUpdateEvents.execute("");
            } catch (IllegalStateException e) {
                // TODO
            }
        }
    }

    private void refreshSplashViews(int messageId, int progress) {

        if (mProgressBar == null || mProgressText == null)
            loadSplashViews();

        mProgressText.setText(messageId);

        if (progress != -1)
            mProgressBar.setProgress(progress);
    }

    private void refreshSplashViews(String message, int progress) {

        if (mProgressBar == null || mProgressText == null)
            loadSplashViews();

        mProgressText.setText(message);

        if (progress != -1)
            mProgressBar.setProgress(progress);
    }

    private void loadSplashViews() {

        mProgressBar = (ProgressBar) findViewById(R.id.splash_progressbar);
        mProgressText = (TextView) findViewById(R.id.splash_progresstext);

        mProgressBar.setVisibility(View.VISIBLE);
        mProgressText.setVisibility(View.VISIBLE);

    }

    private void startUserConfigurationsEmptyDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.splash_dialog_user_config_title)
                .setMessage(R.string.splash_dialog_user_config_message)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sSettingsIntent = new Intent(App.getContext(),
                                        SettingsActivity.class);
                                startActivityForResult(sSettingsIntent, 0);
                            }
                        });

        builder.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(App.getContext(),
                        R.string.splash_dialog_user_config_cancel,
                        Toast.LENGTH_SHORT).show();
                startUserConfigurationsEmptyDialog();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {

        if (isActive) {

            int status = Integer.valueOf((String) event.getSource());
            UPDATE_STATUS flag = UPDATE_STATUS.getUpdateFlag(status);

            switch (flag) {
            case START:
                processEventsUpdating(UPDATE_STATUS.START, event);
                break;
            case UPDATING:
                processEventsUpdating(UPDATE_STATUS.UPDATING, event);
                break;
            case FINISHED:
                processEventsUpdateFinish();
                break;
            case ERROR_INTERNET:
                processInternetError();
                break;
            case ERROR_SERVER:
                processServerError();
                break;
            case ERROR_PARSER:
                processParserError();
                break;
            case ERROR_PLACES:
                processPlacesError();
                break;
            case ERROR_OTHER:
                processOtherError();
                break;
            default:
                break;
            }
        }
    }

    private void processEventsUpdating(UPDATE_STATUS status,
            PropertyChangeEvent event) {

        int progress = -1;

        try {
            progress = Integer.parseInt(event.getPropertyName());
        } catch (NumberFormatException e) {
            progress = mProgressBar.getProgress();
        }

        switch (status) {
        case START:
            refreshSplashViews(R.string.splash_progresstext_starting_update,
                    progress);
            break;
        case UPDATING:

            String text = getResources().getString(
                    R.string.splash_progresstext_events_updating,
                    event.getOldValue(), event.getNewValue());

            refreshSplashViews(text, progress);
            break;
        default:
            break;
        }
    }

    private void processInternetError() {
        if (mController.isUserHasHistory()) {
            callMainActivity(R.string.splash_progresstext_events_in_history);
        } else {
            refreshSplashViews(R.string.splash_dialog_internet_message_fail, -1);
        }
    }

    private void processServerError() {
        if (mController.isUserHasHistory()) {
            callMainActivity(R.string.splash_progresstext_events_in_history);
        } else {
            refreshSplashViews(
                    R.string.splash_progresstext_events_server_error, -1);
        }
    }

    private void processParserError() {
        if (mController.isUserHasHistory()) {
            callMainActivity(R.string.splash_progresstext_events_in_history);
        } else {
            refreshSplashViews(
                    R.string.splash_progresstext_events_parser_error, -1);
        }
    }

    private void processPlacesError() {
        if (mController.isUserHasHistory()) {
            callMainActivity(R.string.splash_progresstext_events_in_history);
        } else {
            refreshSplashViews(
                    R.string.splash_progresstext_events_places_error, -1);
        }
    }

    private void processOtherError() {
        if (mController.isUserHasHistory()) {
            callMainActivity(R.string.splash_progresstext_events_in_history);
        } else {
            refreshSplashViews(R.string.splash_progresstext_events_other_error,
                    -1);
        }
    }

    private void processEventsUpdateFinish() {
        callMainActivity(R.string.splash_progresstext_events_updated);
    }

}