package com.tharin.br.whatsgoingon.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;

import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.util.Utils;

public class EventsActivity extends FragmentActivity implements
        PropertyChangeListener {

    public static final String CITY_KEY = "city";
    public static final String CATEGORY_KEY = "category";
    public static final String POSITION_KEY = "position";

    /**
     * The pager widget, which handles animation and allows swiping horizontally
     * to access previous and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private int mPosition = 0;
    private String mCategory = null;
    private String mCity = null;
    private List<Place> mListPlaces;
    private int mQuantity = 0;

    private boolean isAttached = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        // get extra
        mPosition = getIntent().getExtras().getInt(POSITION_KEY);
        mCategory = getIntent().getExtras().getString(CATEGORY_KEY);
        mCity = getIntent().getExtras().getString(CITY_KEY);

        if (mCategory == null) {
            mListPlaces = Controller.getInstance().getPlacesFromCity(mCity);
        } else {
            mListPlaces = Controller.getInstance().getPlacesFromCategory(
                    mCategory);
        }

        mQuantity = mListPlaces.size();

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.event_view_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        mPager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

                ActionBar mActionbar = getActionBar();

                Place place = mListPlaces.get(arg0);

                mActionbar.setIcon(Utils.getCityDrawable(place.getCity()));
                mActionbar.setTitle(place.getName());

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

        mPager.setCurrentItem(mPosition);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAttached = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isAttached = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.events, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the
            // system to handle the
            // Back button. This calls finish() on this activity and pops the
            // back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment mFragment = EventFragment.newInstance(position, mCategory,
                    mCity);

            return mFragment;
        }

        @Override
        public int getCount() {
            return mQuantity;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String name = mListPlaces.get(position).getName();
            return name;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {

        if (isAttached) {

            System.out.println("Changed property: " + event.getPropertyName()
                    + " [old -> " + event.getOldValue() + "] | [new -> "
                    + event.getNewValue() + "]");
        }

    }

}
