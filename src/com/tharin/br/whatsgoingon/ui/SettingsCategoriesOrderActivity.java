package com.tharin.br.whatsgoingon.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.ui.helper.DynamicListView;
import com.tharin.br.whatsgoingon.ui.helper.StableArrayAdapter;
import com.tharin.br.whatsgoingon.util.AppPreference;
import com.tharin.br.whatsgoingon.util.CategoryComparable;
import com.tharin.br.whatsgoingon.util.Variables;

public class SettingsCategoriesOrderActivity extends Activity {

    private DynamicListView mDynamicList;
    private StableArrayAdapter mStableAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings_category_reorder);

        String[] preferences = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

        if (preferences == null) {
            Toast.makeText(App.getContext(), "Not ready yet",
                    Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        mDynamicList = (DynamicListView) findViewById(R.id.fragment_settings_dynamic_list);

        String[] categories = AppPreference
                .getSettingsPreferences(Variables.SETTINGS_CATEGORIES_KEY);

        ArrayList<String> mCategories = new ArrayList<>();

        for (String category : categories) {
            mCategories.add(category);
        }

        // Order categories to display
        Collections.sort(mCategories, new CategoryComparable());

        mStableAdapter = new StableArrayAdapter(App.getContext(),
                R.layout.reorder_category_layout, mCategories);

        mDynamicList.setCheeseList(mCategories);
        mDynamicList.setAdapter(mStableAdapter);
        mDynamicList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Set<String> mCategoriesOrder = new LinkedHashSet<String>();
        int count = mStableAdapter.getCount();

        for (int i = 0; i < count; i++) {
            mCategoriesOrder.add(i + "_" + mStableAdapter.getItem(i));
        }

        AppPreference.setSettingsPrefrence(
                Variables.SETTINGS_CATEGORIES_ORDER_KEY, mCategoriesOrder);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

}
