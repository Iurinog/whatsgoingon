package com.tharin.br.whatsgoingon.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.controller.UpdateEvents;
import com.tharin.br.whatsgoingon.controller.UpdateEvents.UPDATE_STATUS;
import com.tharin.br.whatsgoingon.ui.helper.DrawerExpandableAdapter;
import com.tharin.br.whatsgoingon.util.CategoryComparable;
import com.tharin.br.whatsgoingon.util.Variables;
import com.tharin.br.whatsgoingon.util.Variables.Category;

public class MainActivity extends FragmentActivity implements
        PropertyChangeListener {

    public static final String TAG = "MainActivity";

    /**
     * Drawer variables
     */
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ExpandableListView mDrawerList;

    private Controller mController;
    private UpdateEvents mUpdateEvents;

    private Intent mEventsIntent;
    private Intent mSettingsIntent;

    private boolean isAttached = false;

    /**
     * The pager widget, which handles animation and allows swiping horizontally
     * to access previous and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private List<String> mCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate Method");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mController = Controller.getInstance();
        mUpdateEvents = UpdateEvents.getInstace();

        mEventsIntent = new Intent(this, EventsActivity.class);
        mSettingsIntent = new Intent(this, SettingsActivity.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAttached = true;
        mUpdateEvents.addChangeListener(this);

        // Drawer
        configureDrawerLayout();

        // Load Main Fragment
        loadMainFragment();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isAttached = false;
        mUpdateEvents.removeChangeListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        // mDrawerToggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == SettingsActivity.CONFIG_RESULT_ADDED) {

            Status mUpdateEventsStatus = mUpdateEvents.getStatus();

            if (mUpdateEventsStatus != Status.RUNNING) {
                try {
                    mUpdateEvents.execute("");
                } catch (IllegalStateException e) {
                    // TODO
                }
            } else {

                mUpdateEvents.cancel(true);

                if (mUpdateEvents.isCancelled())
                    mUpdateEvents.execute("");
            }
        }

        if (resultCode != SettingsActivity.CONFIG_RESULT_NOT_CHANGED) {
            mCategories = new ArrayList<String>();
            if (!mCategories.contains(Category.FAVORITE.name())) {
                mCategories.add(Variables.Category.FAVORITE.name());
            }
            mCategories.addAll(mController.getCategories());

            mPagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {

        if (isAttached) {

            int status = Integer.valueOf((String) event.getSource());
            UPDATE_STATUS flag = UPDATE_STATUS.getUpdateFlag(status);

            switch (flag) {
            case START:
                processStartUpdate();
                break;
            case ERROR_INTERNET:
                processInternetError();
                break;
            case ERROR_SERVER:
                processServerError();
                break;
            case ERROR_PARSER:
                processParserError();
                break;
            case ERROR_OTHER:
                processOtherError();
                break;
            case ERROR_PLACES:
                processPlacesError();
                break;
            case FINISHED:
                processFinishUpdate();
                break;
            default:
                break;
            }
        }
    }

    private void processInternetError() {
        Toast.makeText(App.getContext(), R.string.main_update_internet_fail,
                Toast.LENGTH_LONG).show();
    }

    private void processServerError() {
        Toast.makeText(App.getContext(), R.string.main_update_server_fail,
                Toast.LENGTH_LONG).show();
    }

    private void processParserError() {
        Toast.makeText(App.getContext(), R.string.main_update_parser_fail,
                Toast.LENGTH_LONG).show();
    }

    private void processOtherError() {
        Toast.makeText(App.getContext(), R.string.main_update_other_fail,
                Toast.LENGTH_LONG).show();
    }

    private void processPlacesError() {
        Toast.makeText(App.getContext(),
                R.string.splash_progresstext_events_places_error,
                Toast.LENGTH_LONG).show();
    }

    private void processStartUpdate() {
        Toast.makeText(App.getContext(), R.string.main_update_start,
                Toast.LENGTH_LONG).show();
    }

    private void processFinishUpdate() {
        Toast.makeText(App.getContext(), R.string.main_update_successful,
                Toast.LENGTH_LONG).show();

        onEventsUpdate();
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mUpdateEvents.removeChangeListener(this);
        finishAffinity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        Log.d(TAG, "onMenuItemSelected Method");

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        if (id == R.id.action_refresh) {

            Status mUpdateEventsStatus = mUpdateEvents.getStatus();

            if (mUpdateEventsStatus != Status.RUNNING) {
                mUpdateEvents.execute("");
            }

        } else if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), 1);
        }

        return super.onMenuItemSelected(featureId, item);
    }

    private void configureDrawerLayout() {
        Log.d(TAG, "Configuring Drawer Layout");

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.main_left_drawer);

        List<String> drawerCategories = mController.getCategories();

        if (!drawerCategories.contains(Category.FAVORITE.name())) {
            drawerCategories.add(0, Category.FAVORITE.name());
        }

        DrawerExpandableAdapter mDrawerAdapter = new DrawerExpandableAdapter(
                mController.getCities(), drawerCategories);

        // Set the adapter for the list view
        mDrawerList.setAdapter(mDrawerAdapter);

        // Set the list's click listener
        mDrawerList.setOnChildClickListener(mDrawerChildClickListenner);
        mDrawerList.setOnGroupClickListener(mDrawerGroupClickListenner);
        mDrawerList.setGroupIndicator(null);

        // Expand all groups
        int groupCount = mDrawerAdapter.getGroupCount();

        for (int i = 0; i < groupCount; i++) {
            mDrawerList.expandGroup(i);
        }

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.app_name, R.string.app_name);

        // {
        // public void onDrawerOpened(View drawerView) {
        // super.onDrawerOpened(drawerView);
        // configureDrawerLayout();
        // }
        // }

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    private void loadMainFragment() {
        Log.d(TAG, "Loading Main Fragment");

        mCategories = new ArrayList<String>();
        mCategories.addAll(mController.getCategories());

        // Order list according user preference
        Collections.sort(mCategories, new CategoryComparable());

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.event_view_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    private void onEventsUpdate() {

        // Drawer
        configureDrawerLayout();

        // Refresh Main Fragment
        loadMainFragment();
    }

    private OnChildClickListener mDrawerChildClickListenner = new OnChildClickListener() {

        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                int groupPosition, int childPosition, long id) {

            switch (groupPosition) {
            case 0:

                // Events
                String category = mController.getCategories()
                        .get(childPosition);

                mEventsIntent.putExtra(EventsActivity.CATEGORY_KEY, category);
                mEventsIntent.putExtra(EventsActivity.POSITION_KEY,
                        childPosition);
                startActivity(mEventsIntent);

                break;

            case 1:

                TextView mCityTextView = (TextView) v
                        .findViewById(R.id.drawer_child_name);

                mEventsIntent.putExtra(EventsActivity.CITY_KEY,
                        mCityTextView.getText());
                startActivity(mEventsIntent);
                break;
            case 2:

                startActivity(mSettingsIntent);
                break;
            default:
                break;
            }

            return true;
        }
    };

    private OnGroupClickListener mDrawerGroupClickListenner = new OnGroupClickListener() {

        @Override
        public boolean onGroupClick(ExpandableListView parent, View v,
                int groupPosition, long id) {
            return true;
        }
    };

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment mFragment = MainFragment.newInstance(mCategories
                    .get(position));

            return mFragment;
        }

        @Override
        public int getCount() {
            return mCategories.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mCategories.get(position);
        }

        public int getItemPosition(Object item) {
            return POSITION_NONE;
        }
    }

}
