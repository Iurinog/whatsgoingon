package com.tharin.br.whatsgoingon.receiver;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask.Status;
import android.support.v4.app.NotificationCompat;

import com.tharin.br.whatsgoingon.App;
import com.tharin.br.whatsgoingon.R;
import com.tharin.br.whatsgoingon.controller.Controller;
import com.tharin.br.whatsgoingon.controller.UpdateEvents;
import com.tharin.br.whatsgoingon.controller.UpdateEvents.UPDATE_STATUS;
import com.tharin.br.whatsgoingon.model.Place;
import com.tharin.br.whatsgoingon.ui.SplashActivity;
import com.tharin.br.whatsgoingon.util.AppPreference;
import com.tharin.br.whatsgoingon.util.InternetConnection;
import com.tharin.br.whatsgoingon.util.InternetConnection.INTERNET_STATUS;
import com.tharin.br.whatsgoingon.util.Utils;
import com.tharin.br.whatsgoingon.util.Variables;

public class NetworkChangeReceiver extends BroadcastReceiver implements
        PropertyChangeListener {

    public static final String TAG = "NetworkChangeReceiver";
    public static final int ID = 103596;

    private boolean isAppRunning = false;

    private static Intent resultIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        // Set the Context for all application
        App.setContext(context.getApplicationContext());

        if (isThereAnyPlaceRegistered())
            return;

        if (NotificationAlreadyShowedToday())
            return;

        if (InternetConnection.getConnectivityStatus() != INTERNET_STATUS.TYPE_NOT_CONNECTED) {

            UpdateEvents mUpdateEvents = UpdateEvents.getInstace();
            mUpdateEvents.addChangeListener(this);

            Status mUpdateEventsStatus = mUpdateEvents.getStatus();

            if (mUpdateEventsStatus != Status.RUNNING) {
                mUpdateEvents.execute("");
            }
        }
    }

    /**
     * Check if user already registered his places
     */
    private boolean isThereAnyPlaceRegistered() {
        Controller mController = Controller.getInstance();
        List<Place> mPlaces = mController.getPlaces();

        return mPlaces != null && !mPlaces.isEmpty();
    }

    /**
     * Check if notification was showed today TODO validate
     * 
     * @return
     */
    private boolean NotificationAlreadyShowedToday() {

        String lastDate = AppPreference
                .getStringPreference(Variables.RECEIVER_NOTIFICATION);

        if (lastDate.isEmpty())
            return false;

        return Utils.compareDates(lastDate);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {

        isAppRunning = UpdateEvents.getInstace().isAppRunning();

        // TODO validate
        if (!isAppRunning) {

            int status = Integer.valueOf((String) event.getSource());
            UPDATE_STATUS flag = UPDATE_STATUS.getUpdateFlag(status);

            if (flag == UPDATE_STATUS.FINISHED) {
                Resources resoruces = App.getContext().getResources();
                Uri soundNotification = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        App.getContext())
                        .setVibrate(new long[] { 500 })
                        .setAutoCancel(true)
                        .setSound(soundNotification)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(
                                resoruces
                                        .getString(R.string.notification_title))
                        .setContentText(
                                resoruces
                                        .getString(R.string.notification_message));

                resultIntent = new Intent(App.getContext(),
                        SplashActivity.class);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(
                        App.getContext(), 0, resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.setContentIntent(resultPendingIntent);

                NotificationManager mNotificationManager = (NotificationManager) App
                        .getContext().getSystemService(
                                Context.NOTIFICATION_SERVICE);
                // mId allows you to update the notification later on.
                mNotificationManager.notify(ID, mBuilder.build());
            }
        }
    }
}